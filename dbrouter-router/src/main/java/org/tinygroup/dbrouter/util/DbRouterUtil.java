/**
 *  Copyright (c) 1997-2013, tinygroup.org (luo_guo@live.cn).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * --------------------------------------------------------------------------
 *  版权 (c) 1997-2013, tinygroup.org (luo_guo@live.cn).
 *
 *  本开源软件遵循 GPL 3.0 协议;
 *  如果您不遵循此协议，则不被允许使用此文件。
 *  你可以从下面的地址获取完整的协议文本
 *
 *       http://www.gnu.org/licenses/gpl.html
 */
package org.tinygroup.dbrouter.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.tinygroup.commons.beanutil.BeanUtil;
import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.dbrouter.config.Router;
import org.tinygroup.dbrouter.config.DataSourceConfig;
import org.tinygroup.dbrouter.factory.RouterManagerBeanFactory;
import org.tinygroup.jsqlparser.expression.Expression;
import org.tinygroup.jsqlparser.expression.JdbcParameter;
import org.tinygroup.jsqlparser.expression.LongValue;
import org.tinygroup.jsqlparser.expression.StringValue;
import org.tinygroup.jsqlparser.expression.operators.relational.ExpressionList;
import org.tinygroup.jsqlparser.expression.operators.relational.ItemsList;
import org.tinygroup.jsqlparser.schema.Column;
import org.tinygroup.jsqlparser.schema.Table;
import org.tinygroup.jsqlparser.statement.Statement;
import org.tinygroup.jsqlparser.statement.delete.Delete;
import org.tinygroup.jsqlparser.statement.insert.Insert;
import org.tinygroup.jsqlparser.statement.select.AllColumns;
import org.tinygroup.jsqlparser.statement.select.AllTableColumns;
import org.tinygroup.jsqlparser.statement.select.FromItem;
import org.tinygroup.jsqlparser.statement.select.Join;
import org.tinygroup.jsqlparser.statement.select.OrderByElement;
import org.tinygroup.jsqlparser.statement.select.PlainSelect;
import org.tinygroup.jsqlparser.statement.select.Select;
import org.tinygroup.jsqlparser.statement.select.SelectBody;
import org.tinygroup.jsqlparser.statement.select.SelectExpressionItem;
import org.tinygroup.jsqlparser.statement.select.SelectItem;
import org.tinygroup.jsqlparser.statement.select.SetOperationList;
import org.tinygroup.jsqlparser.statement.select.WithItem;
import org.tinygroup.jsqlparser.statement.update.Update;

/**
 * 功能说明: 工具类
 * <p/>
 * 
 * 开发人员: renhui <br>
 * 开发时间: 2013-12-17 <br>
 * <br>
 */
public class DbRouterUtil {


	/**
	 * 
	 * 替换sql语句中的表名信息，条件语句带表名，暂时不进行替换。
	 * @param sql
	 * @param tableMapping
	 * @return
	 */
	public static String transformSqlWithTableName(String sql,
			Map<String, String> tableMapping) {
		try {
			Statement originalStatement = RouterManagerBeanFactory
					.getManager().getSqlStatement(sql);
			Statement statement = (Statement) BeanUtil
					.deepCopy(originalStatement);
			if (statement instanceof Insert) {
				Insert insert = (Insert) statement;
				String tableName = insert.getTable().getName();
				String newTableName = tableMapping.get(tableName);
				if (!StringUtil.isBlank(newTableName)) {
					insert.getTable().setName(newTableName);
					List<Column> columns=insert.getColumns();
					for (Column column : columns) {//变化新增字段信息
						Table columnTable=column.getTable();
						if(columnTable!=null&&!StringUtil.isBlank(columnTable.getName())){
							columnTable.setName(newTableName);
						}
					}
					return insert.toString();
				}
			}
			if (statement instanceof Delete) {
				Delete delete = (Delete) statement;
				String tableName = delete.getTable().getName();
				String newTableName = tableMapping.get(tableName);
				if (!StringUtil.isBlank(newTableName)) {
					delete.getTable().setName(newTableName);
					return delete.toString();
				}
			}
			if (statement instanceof Update) {
				Update update = (Update) statement;
				String tableName = update.getTable().getName();
				String newTableName = tableMapping.get(tableName);
				if (!StringUtil.isBlank(newTableName)) {
					update.getTable().setName(newTableName);
					return update.toString();
				}
			}
			if (statement instanceof Select) {
				Select select = (Select) statement;
				SelectBody body = select.getSelectBody();
				if (body instanceof PlainSelect) {
					transformPlainSelect(tableMapping, body);
				}
				if (body instanceof SetOperationList) {
					SetOperationList operationList = (SetOperationList) body;
					List<PlainSelect> plainSelects = operationList
							.getPlainSelects();
					for (PlainSelect plainSelect : plainSelects) {
						transformPlainSelect(tableMapping, plainSelect);
					}
				}
				if (body instanceof WithItem) {
					WithItem withItem = (WithItem) body;
					PlainSelect plainSelect = (PlainSelect) withItem
							.getSelectBody();
					transformPlainSelect(tableMapping, plainSelect);
				}
				return select.toString();
			}
			return sql;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static int getSqlParamSize(String sql) {
		int paramSize = 0;
		int len = sql.length() + 1;
		char[] command = new char[len];
		len--;
		sql.getChars(0, len, command, 0);
		for (int i = 0; i < len; i++) {
			char c = command[i];
			if (c == '?') {
				paramSize++;
			}
		}
		return paramSize;
	}

	private static void transformPlainSelect(Map<String, String> tableMapping,
			SelectBody body) {
		PlainSelect plainSelect = (PlainSelect) body;
		checkOrderByAndGroupbyItem(plainSelect);
		Table table = (Table) plainSelect.getFromItem();
		String tableName = table.getName();
		String newTableName = tableMapping.get(tableName);
		if (!StringUtil.isBlank(newTableName)) {
			table.setName(newTableName);
		}
		List<Join> joins = plainSelect.getJoins();
		if (joins != null) {
			for (Join join : joins) {
				Table joinTable = (Table) join.getRightItem();
				String joinTableName = joinTable.getName();
				String newJoinTableName = tableMapping.get(joinTableName);
				if (!StringUtil.isBlank(newJoinTableName)) {
					joinTable.setName(newJoinTableName);
				}
			}
		}
	}

	/**
	 * 框架内部会对sql进行处理：
	 * 如果是insert语句，会检测主键字段是否有传值。如果插入语句没有传主键字段，将会自动赋值。
	 * @param sql
	 * @param router
	 * @param metaData
	 * @return
	 */
	public static String transformInsertSql(String sql, Router router,Map<String, String> tableMapping,
			DatabaseMetaData metaData) {
		Statement originalStatement = RouterManagerBeanFactory.getManager()
				.getSqlStatement(sql);
		ResultSet rs = null;
		ResultSet typeRs = null;
		try {
			Statement statement = (Statement) BeanUtil
					.deepCopy(originalStatement);
			if (statement instanceof Insert) {
				Insert insert = (Insert) statement;
				Table table = insert.getTable();
				String tableName = table.getName();
				String realTableName=tableName;//真正在数据库存在的表名
				if(tableMapping!=null){
					if(tableMapping.containsKey(tableName)){
						realTableName=tableMapping.get(tableName);
					}
				}
				rs = metaData.getPrimaryKeys(null, null, realTableName);
				if (rs.next()) {
					String primaryKey = rs.getString("COLUMN_NAME");
					typeRs = metaData.getColumns(null, null, realTableName,
							primaryKey);
					if(typeRs.next()){
						int dataType = typeRs.getInt("DATA_TYPE");
						List<Column> columns = insert.getColumns();
						boolean exist = primaryKeyInColumns(primaryKey, columns);
						if (!exist) {
							Column primaryColumn = new Column(table, primaryKey);
							columns.add(primaryColumn);
							ItemsList itemsList = insert.getItemsList();
							if (itemsList instanceof ExpressionList) {
								List<Expression> expressions = ((ExpressionList) itemsList)
										.getExpressions();
								Expression expression = null;
								switch (dataType) {
								case Types.CHAR:
								case Types.VARCHAR:
								case Types.LONGVARCHAR:
									String value = RouterManagerBeanFactory
											.getManager().getPrimaryKey(router,
													tableName);
									StringValue stringValue = new StringValue();
									stringValue.setValue(value);
									expression = stringValue;
								case Types.NUMERIC:
								case Types.DECIMAL:
								case Types.INTEGER:
								case Types.SMALLINT:
								case Types.TINYINT:
								case Types.BIGINT:
									Object values = RouterManagerBeanFactory
											.getManager().getPrimaryKey(router,
													tableName);
									expression = new LongValue(values + "");
								default:
								}
								expressions.add(expression);
							}
							return insert.toString();
						}
					}
				}

			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (typeRs != null) {
					typeRs.close();
				}
			} catch (Exception e) {

			}

		}
		return sql;
	}
	
	
	private static boolean primaryKeyInColumns(String primaryKey,
			List<Column> columns) {
		boolean exist = false;
		if (!CollectionUtil.isEmpty(columns)) {
			for (Column column : columns) {
				if (column.getColumnName().equals(primaryKey)) {
					exist = true;
					break;
				}
			}
		}
		return exist;
	}

	/**
	 * 检测查询语句选择项是否包含orderby字段，不存在则创建orderby字段
	 * 
	 * @param plainSelect
	 */
	public static void checkOrderByAndGroupbyItem(PlainSelect plainSelect) {
		List<Column> orderByColumns = getOrderByColumns(plainSelect);
		List<Column> groupByColumns = getGroupByColumns(plainSelect);
		List<SelectItem> selectItems = plainSelect.getSelectItems();
		for (Column groupByColumn : groupByColumns) {
			checkItem(selectItems, groupByColumn);
		}
		for (Column orderByColumn : orderByColumns) {
			checkItem(selectItems, orderByColumn);
		}
	}

	public static void checkItem(List<SelectItem> selectItems,
			Column checkColumn) {
		String groupByName = checkColumn.getWholeColumnName();
		boolean isExist = false;
		for (SelectItem selectItem : selectItems) {
			if (selectItem instanceof AllColumns) {
				isExist = true;
				break;
			}
			if (selectItem instanceof AllTableColumns) {
				isExist = true;
				break;
			}
			SelectExpressionItem item = (SelectExpressionItem) selectItem;
			String alias = item.getAlias();
			if (alias != null && groupByName.equals(alias)) {
				isExist = true;
				break;
			}
			Expression expression = item.getExpression();
			if (expression instanceof Column) {
				Column column = (Column) expression;
				if (groupByName.equals(column.getWholeColumnName())
						|| groupByName.equals(column.getColumnName())) {
					isExist = true;
					break;
				}
			}
		}
		if (!isExist) {
			SelectExpressionItem item = new SelectExpressionItem();
			Column column = new Column(checkColumn.getTable(),
					checkColumn.getColumnName());
			item.setExpression(column);
			selectItems.add(item);
		}

	}

	/**
	 * 获取查询语句orderby字段的位置
	 * 
	 * @param plainSelect
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	public static int[] getOrderByIndexs(PlainSelect plainSelect,
			ResultSet resultSet) throws SQLException {

		List<Column> orderByColumns = getOrderByColumns(plainSelect);
		if (CollectionUtil.isEmpty(orderByColumns)) {
			return new int[0];
		}
		List<SelectItem> selectItems = plainSelect.getSelectItems();
		int[] indexs = new int[orderByColumns.size()];
		for (int i = 0; i < orderByColumns.size(); i++) {
			Column orderByColumn = orderByColumns.get(i);
			String orderByName = orderByColumn.getWholeColumnName();
			for (int j = 0; j < selectItems.size(); j++) {
				SelectItem selectItem = selectItems.get(j);
				if (selectItem instanceof AllColumns
						|| selectItem instanceof AllTableColumns) {
					indexs[i] = resultSet.findColumn(orderByName) - 1;
					break;
				}
				SelectExpressionItem item = (SelectExpressionItem) selectItem;
				String alias = item.getAlias();
				if (alias != null && orderByName.equals(alias)) {
					indexs[i] = j;
					break;
				}
				Expression expression = item.getExpression();
				if (expression instanceof Column) {
					Column column = (Column) expression;
					if (orderByName.equals(column.getWholeColumnName())
							|| orderByName.equals(column.getColumnName())) {
						indexs[i] = j;
					}
				}
			}
		}
		return indexs;
	}

	public static List<Column> getOrderByColumns(PlainSelect plainSelect) {
		List<Column> orderByColumns = new ArrayList<Column>();
		List<OrderByElement> orderByElements = plainSelect.getOrderByElements();
		if (!CollectionUtil.isEmpty(orderByElements)) {
			for (OrderByElement orderByElement : orderByElements) {
				Column column = (Column) orderByElement.getExpression();
				orderByColumns.add(column);
			}
		}
		return orderByColumns;
	}

	public static List<Column> getGroupByColumns(PlainSelect plainSelect) {
		List<Column> groupByColumns = new ArrayList<Column>();
		List<Expression> groupExpressions = plainSelect
				.getGroupByColumnReferences();
		if (!CollectionUtil.isEmpty(groupExpressions)) {
			for (Expression expression : groupExpressions) {
				Column column = (Column) expression;
				groupByColumns.add(column);
			}
		}
		return groupByColumns;
	}

	public static String getSelectTableName(String sql) {
		Statement statement = RouterManagerBeanFactory.getManager()
				.getSqlStatement(sql);
		if (statement instanceof Select) {
			Select select = (Select) statement;
			SelectBody body = select.getSelectBody();
			if (body instanceof PlainSelect) {
				return getTableNameWithPlainSelect((PlainSelect) body);
			}
		}
		throw new RuntimeException("must be a query sql");

	}

	private static String getTableNameWithPlainSelect(PlainSelect plainSelect) {
		FromItem fromItem = plainSelect.getFromItem();
		if (fromItem instanceof Table) {
			Table table = (Table) fromItem;
			return table.getName();
		}
		return null;
	}

	public static Connection createConnection(DataSourceConfig config)
			throws SQLException {
		try {
			Class.forName(config.getDriver());
			Connection connection = DriverManager
					.getConnection(config.getUrl(), config.getUserName(),
							config.getPassword());
			// TODO 这里暂时设置成自动提交
			connection.setAutoCommit(true);
			return connection;

		} catch (ClassNotFoundException e) {
			throw new SQLException(e.getMessage());
		}
	}

	/**
	 * 
	 * 检查参数指定的字段名称在列集合元素的位置，返回-1说明columnName不存在columns中
	 * 
	 * @param columnName
	 * @param columns
	 * @return
	 */
	public static int checkColumnIndex(String columnName, List<Column> columns) {
		if (!CollectionUtil.isEmpty(columns)) {
			for (int i = 0; i < columns.size(); i++) {
				Column column = columns.get(i);
				String name = column.getColumnName();
				int index = name.indexOf(".");
				if (index != -1) {
					name = name.substring(index + 1);
				}
				if (name.equals(columnName)) {
					return i+1;
				}
			}
		}
		return -1;
	}

	/**
	 * 
	 * 检查insert语句中列号为columnIndex的jdbcParamter的位置，例如 insert into aaa(id,name,age)
	 * values(11,?,?) columnIndex:<=1----return -1 columnIndex:2----return 0;
	 * columnIndex:3----return 1; columnIndex:>3-----return -1;
	 * 
	 * @param columnIndex
	 * @param expressions
	 * @return
	 */
	public static int checkParamIndex(int columnIndex,
			List<Expression> expressions) {
		if (CollectionUtil.isEmpty(expressions)
				|| columnIndex > expressions.size() || columnIndex < 1) {
			return -1;
		}
		int paramIndex = -1;
		for (int i = 0; i < columnIndex; i++) {
			Expression expression = expressions.get(i);
			if (expression instanceof JdbcParameter) {
				paramIndex++;
			}
		}
		return paramIndex;
	}

}
